# Install Displaylink with latest Linux kernel compatibility

sudo dnf install libdrm-devel.x86_64 libdrm.x86_64 kernel-devel-$(uname -r)
git clone https://github.com/DisplayLink/evdi.git
cd evdi
make
sudo make install
cd ..
wget https://github.com/displaylink-rpm/displaylink-rpm/releases/download/v5.4.0-1/fedora-34-displaylink-1.9.1-1.x86_64.rpm
sudo dnf install fedora-34-displaylink-1.9.1-1.x86_64.rpm
