#!/bin/bash
# Script de post-installation pour Fedora 35

# System update
sudo dnf upgrade --refresh

sudo dnf install -y google-chrome-stable
sudo dnf install -y lpf-spotify-client
sudo dnf install -y inkscape
sudo dnf install -y dconf-editor
sudo dnf install -y gnome-tweak-tool
sudo dnf install -y gparted
sudo dnf install -y gimp
sudo dnf install -y git-lfs
sudo dnf install -y guvcview

# Docker
sudo dnf install -y docker
groupadd docker
sudo usermod -aG docker $USER
newgrp docker

# VSCode
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf check-update
sudo dnf install -y code

# Slack
sudo dnf install -y https://downloads.slack-edge.com/releases/linux/4.23.0/prod/x64/slack-4.23.0-0.1.fc21.x86_64.rpm

# Keybase
sudo dnf install -y https://prerelease.keybase.io/keybase_amd64.rpm

# Gitlab-runner
sudo dnf install -y https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm

# Zoom
sudo dnf install -y https://zoom.us/client/latest/zoom_x86_64.rpm

# WhiteSur Theme
sudo dnf install -y sassc optipng ImageMagick glib2-devel
git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git
cd WhiteSur-gtk-theme
sudo ./install.sh -s 220 -i fedora
sudo ./tweaks.sh -g


sudo wget -P /usr/share/backgrounds https://raw.githubusercontent.com/vinceliuice/WhiteSur-gtk-theme/wallpapers/1080p/WhiteSur-light.jpg
gsettings set org.gnome.desktop.background picture-uri file:///usr/share/backgrounds/WhiteSur-light.jpg

# Parirus Icon Theme
wget -qO- https://git.io/papirus-icon-theme-install | sudo bash

# Wifi disconnected when ethernet is active
sudo cp wifi-wired-exclusive.sh /etc/NetworkManager/dispatcher.d/
sudo chmod a+rx /etc/NetworkManager/dispatcher.d/wifi-wired-exclusive.sh

# Toubleshoot gne ext install
# export XDG_SESSION_TYPE=x11
# exec gnome-session

# Gif recorder
https://flathub.org/apps/details/io.github.seadve.Kooha

# Patch chrome OS grid icon from Papirus icon to Whitesur theme
sudo cp /usr/share/icons/Papirus/64x64/categories/chrome-app-list.svg /usr/share/themes/WhiteSur-light/gnome-shell/assets/view-app-grid.svg
