# WARNING: Ensure to activate MesloLGS in terminal configuration and in VS code

# Install zsh and antigen
sudo apt install zsh
sudo mkdir /usr/share/zsh-antigen
sudo curl -o /usr/share/zsh-antigen/antigen.zsh -sL git.io/antigen

# Set default shell
chsh -s $(which zsh)

# Download fonts
sudo wget -P /usr/local/share/fonts https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
sudo wget -P /usr/local/share/fonts https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
sudo wget -P /usr/local/share/fonts https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
sudo wget -P /usr/local/share/fonts https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
